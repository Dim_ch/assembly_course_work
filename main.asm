text segment public 'code'
	assume cs:text,ds:data
	public x,y,color
	extrn set_v_mode:proc,set_txt_mode:proc
	extrn star:proc,star_hide:proc,flash:proc,manageFlash:proc
	extrn move_l:proc,move_r:proc,move_u:proc,move_d:proc

main proc
	mov ax,data
	mov ds,ax
	
	call set_v_mode
	call star
	                            ;Ждёт пока не нажмут клавишу, если нажали,
	                            ;то продолжается выполнение программы
	mov bx,0                    ;флаг для смены цвета
	cycle:
		call flash
		
		mov ah,06h
		mov dl,0ffh
		int 21h
		jz cycle
                                ;Если не функциональная клавиша,
								;то слушать заново
		cmp al,0
		jne lab4;
		int 21h
		;Нажали на F1
		cmp al,59
		je exit
		
		cmp al,75           	;left
        jne lab1
		call move_l
		
		lab1:
        cmp al,77           	;right
        jne lab2
		call move_r
		
		lab2:
        cmp al,72           	;up
        jne lab3
		call move_u
		
		lab3:
        cmp al,80           	;down
        jne lab4
		call move_d
		
		lab4:
			call manageFlash
			jmp cycle
		exit:
		call set_txt_mode
		mov ax,4c00h        	;exit
		int 21h
main endp

data segment
    x dw 50						;Координаты x и y
    y dw 50						;- это центр фигуры.
	color db 33					;33-247
data ends

stk segment stack
    dw 128 dup (0)
stk ends

text ends
	end main