text segment public 'code'
    assume cs:text
    public set_v_mode,set_txt_mode,star,star_hide,move_d,move_l,move_r,move_u,flash,manageFlash
	extrn x:word
    extrn y:word
	extrn color:byte
	
set_v_mode proc
    mov ah,4fh     ;seting video mode
    mov al,02h
	mov bx,103h    ;800x600x256
    int 10h
    ret
set_v_mode endp

set_txt_mode proc
    mov ah,00h          ;seting video mode
    mov al,03h          ;set text mode
    int 10h
    ret
set_txt_mode endp

horizontal proc
	d1:
        push cx
        mov cx,si
        int 10h
        pop cx
        inc si		;x
        loop d1
	ret
horizontal endp

vertical proc
    d2:
        push cx
        mov cx,si
        int 10h
        pop cx
        inc dx		;y
        loop d2
	ret
vertical endp

;draw line TopLeft to BottomRight
LineToBottomRight proc
	d3:
        push cx
        mov cx,si
        int 10h
        pop cx
        inc dx
		inc si
        loop d3
	ret
LineToBottomRight endp

;draw line BottomLeft to TopRight
LineToTopRight proc
	d4:
        push cx
        mov cx,si
        int 10h
        pop cx
        dec dx 	
		inc si
        loop d4
	ret
LineToTopRight endp

check_color proc
	cmp al,105
	jng ex5
	mov al,33
	
	ex5:
		ret
check_color endp

star proc
	push ax
	push bx
	push cx
	push si
	push dx
	
	mov al,color
	mov ah,0ch          ;draw pixel
    mov bh,0			;ViewPage
	mov cx,50			;length
	mov dx,y			;y
    mov si,x			;x
	sub si,cx
	call horizontal
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	sub si,cx
	sub dx,cx
	call LineToBottomRight
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	sub dx,cx
	call vertical
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	call LineToTopRight
	
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	call horizontal
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	call LineToBottomRight
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	call vertical
	
	inc al
	call check_color
	mov cx,50
	mov dx,y
	mov si,x
	sub si,cx
	add dx,cx
	call LineToTopRight
	mov color,al
	
	pop dx
	pop si
	pop cx
	pop bx
	pop ax
    ret
star endp

star_hide proc
	push ax
	push bx
	push cx
	push si
	push dx
	
	mov al,0
	mov ah,0ch          ;draw pixel
    mov bh,0			;ViewPage
	mov cx,50			;length
	mov dx,y			;y
    mov si,x			;x
	sub si,cx
	call horizontal
	
	mov cx,50
	mov dx,y
	mov si,x
	sub si,cx
	sub dx,cx
	call LineToBottomRight
	
	mov cx,50
	mov dx,y
	mov si,x
	sub dx,cx
	call vertical

	mov cx,50
	mov dx,y
	mov si,x
	call LineToTopRight

	mov cx,50
	mov dx,y
	mov si,x
	call horizontal

	mov cx,50
	mov dx,y
	mov si,x
	call LineToBottomRight

	mov cx,50
	mov dx,y
	mov si,x
	call vertical

	mov cx,50
	mov dx,y
	mov si,x
	sub si,cx
	add dx,cx
	call LineToTopRight
	
	pop dx
	pop si
	pop cx
	pop bx
	pop ax
    ret
star_hide endp

move_l proc
	cmp x,50
	je ex1
	call star_hide
	sub x,50
	call star
    ex1:
        ret
move_l endp
;Передвижение вправо
move_r proc
	cmp x,750
	je ex1
	call star_hide
	add x,50
	call star
    ex2:
        ret
move_r endp
;Передвижение вверх
move_u proc
	cmp y,50
	je ex3
	call star_hide
	sub y,50
	call star
    ex3:
        ret
move_u endp
;Передвижение вниз
move_d proc
	cmp y,550
	je ex4
	call star_hide
	add y,50
	call star
    ex4:
        ret
move_d endp

;меняет значение bx на обратное
;устанавливает в si,di начало отсчёта
manageFlash proc
	cmp bx,0
	je	ex6
	mov bx,0
	jmp ex7
	ex6:
		mov bx,1
		mov ah,00h
		int 1ah
		mov si,dx
		mov di,dx
	ex7:
		ret
manageFlash endp

;контроль мигания
flash proc
	cmp bx,1
	jne lab0
	mov ah,00h
	int 1ah
	sub dx,si
	;через 8 секунд мигание
	;фигуры прекращается
	cmp dx,144
	jge lab
	int 1ah
	sub dx,di
	;фигура мигает каждые 222 милисекунды
	cmp dx,4
	jg lab00
	jmp lab0
	
	lab00:
		call star
		int 1ah
		mov di,dx
		jmp lab0
	
	lab:
		mov bx,0
	
	lab0:
	ret
flash endp

text ends
    end